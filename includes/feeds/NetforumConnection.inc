<?php

/**
 * @file
 * Home of the Netforum Feeds and related classes.
 */

// netforum will only return a maximum of 500 results at a time when using certain
// functions. this means we'll have to iterate in batches. this value 
define('NETFORUM_MAX_RESULTS', 500);

/**
 * Result of FeedsHTTPFetcher::fetch().
 */
class NetforumConnectionResult extends FeedsFetcherResult {
  /**
   * Constructor.
   */
  public function __construct($object_type, $arguments) {
    $this->object_type=$object_type;
    $this->arguments=$arguments;
    
    parent::__construct('');
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    // prevent this from running successive times
    if(!isset($this->response)){
      $this->response = netforum_xweb_request($this->object_type, $this->arguments);
      
      // reduce the returned response to max_results. this is usefull for testing date iterations
      // wont be practical in realworld scenarios
      $test=(array)$this->response;
      if(count($test['Result']) > NETFORUM_MAX_RESULTS){
        $test['Result']=array_slice($test['Result'], 0, NETFORUM_MAX_RESULTS);
        $this->response=$test;
      }
    }
    
    return $this->response;
  }
}

/**
 * Fetches data from Netforum.
 */
class NetForumConnection extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $state=$source->state(FEEDS_FETCH);
  
    // get the configuration for this xweb request
    $config=$this->config[$this->config['retrieve_object']];

    // if the feed is configured to batch-iterate set it up here
    if($config['iterate']){
      if(!isset($state->iterate)){
        // iteration can happen across several grouped fields
        // we need to prepare for that, but for now there's just one field
        $state->iterate=array();
        
        //TODO: eventually we will need a foreach loop here to set this up correctly
        
        // set the default iteration values
        $state->iterate[$config['iterate']]=$config['attributes'][$config['iterate']];
        
        // modify the iterate config element's start variable to match batch start date.
        // that value changes incrementally as the data comes in, and is modified at the
        // end of the function
      }
      
      // iteration is going to happen, populate iteratable values into config attributes
      // before the query is run
      
      //TODO: this will need a foreach loop too
      $config['attributes'][$config['iterate']]=$state->iterate[$config['iterate']];
    }
    
    // get the data needed here
    $fetcher=new NetforumConnectionResult($this->config['retrieve_object'], $config['attributes']);
    
    $data=(array)$fetcher->getRaw();
    
    if($config['iterate']){
      // handle alphabetical iteration first
      if($config['iterate_type']=='alphabetical'){
        if($state->iterate[$config['iterate']] < 'z'){
          //increment to the next letter for searching
          $state->iterate[$config['iterate']]=chr(ord(strtolower($state->iterate[$config['iterate']]))+1);
          $state->progress(ord('z')-ord('a'), ord($state->iterate[$config['iterate']])-ord('a'));
          
        }
      }
      elseif($config['iterate_type']=='date_field'){
        //get the mappings
        $processor_config=$source->importer->processor->getConfig();
        
        foreach($processor_config['mappings'] as $index=>$mapelement){
          if($mapelement['target'] == 'iteratorField'){
            $iterator_field=$mapelement['source'];
            break;
          }
        }
        // if the iterator field has been defined
        if($iterator_field){
          //TODO: confirm that the last record is the newest.
          $last_row=count($data['Result'])-1;
          //correct the starting date so proper(ish) batch completion progress can be reported
          if(!isset($state->batch_start_date)){
            
            // note: using unix timestamps
            $state->batch_start_date=strtotime($data['Result'][0]->{$iterator_field});
          }
          
          // if netforum returned the maximum records allowed for a date query
          // another batch iteration needs to take place.
          if(count($data['Result']) >= NETFORUM_MAX_RESULTS){
            //set the next query-by date to the date of the youngest record in the set
//            $last_row_data=(array)$data['Result'][$last_row];
            $state->iterate[$config['iterate']]=(string)$data['Result'][$last_row]->{$iterator_field};
            
            // progress total = now() - earliest_record_date
            // elapsed = next_starting point - earliest_record_date
            $now=strtotime('now');
            $state->progress($now - $state->batch_start_date, strtotime($state->iterate[$config['iterate']]) - $state->batch_start_date);
//            $state->progress(1,1);
          }
          // if the record count is w/in the proper range, progress is complete
          else{
            //we're done
            $state->progress(1,1);
          }
          
        }
        
      }
      else{
        // another iterator of some sort
      }
    }
    
    return $fetcher;
  }
  
  
  
  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $defaults=array(
      'retrieve_object' => '',
    );
    
    foreach(array_keys(_netforum_xweb_soap_functions()) as $function){
      $defaults[$function]=array(
        'iterate'=>'',
        'iterate_type'=>'',
        'attributes'=>array(),
      );
      foreach( netforum_xweb_function_struct_parameters($function) as $param){
        if(!is_array($param)){
          $defaults[$function]['attributes'][$param]='';
        }
      }
    }
    
    return $defaults;
  }
  
  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array('#tree'=>true);
    $form['retrieve_object'] = array(
        '#type' => 'select',
        '#title' => t('Object Type to Retrieve'),
        '#description' => t('What kind of Object is this'),
        '#default_value' => $this->config['retrieve_object'],
        '#options'=> array(
        ),
    );
    // Get the soap function list and create fields for each function's params    
    $function_list = _netforum_xweb_soap_functions();
    
    foreach (array_keys($function_list) as $function_name) {
      // register the function with the options selection.
      $form['retrieve_object']['#options'][$function_name]=$function_name;
      // iterate through the params list and define any fields that haven't already
      // been defined.
      $form[$function_name]=array(
        '#type'=>'fieldset',
        '#title'=>t('Params for %function', array('%function'=>$function_name)),
        '#dependency'=>array(
          'edit-retrieve-object'=>array($function_name),
        ),
      );
      $params = netforum_xweb_function_struct_parameters($function_name);
//      dpm($params, $function_name);
      $iterate_options=array();
      foreach($params as $key=>$param){
        if(!is_array($param)){
          //the param types are in the name of the function
          // this should switch between those types to provide better field entry
          // do this later.
          $form[$function_name]['attributes'][$param]=array(
            '#type'=>'textfield',
            '#title'=>$param,
            '#default_value'=>$this->config[$function_name]['attributes'][$param],
            '#description'=>""
          );
//all params are iteratable. date or alpha are options at the moment          
//          if(preg_match('/[a-z]Date/', $param)){
            // register this date field as a possible iterator field
            $iterate_options[$param]=$param;
//          }
        }
      }
      if(count($iterate_options)){
        $form[$function_name]['iterate']=array(
          '#type'=>'checkboxes',
          '#title'=>t('iterate using this date field'),
          '#default_value'=>$this->config[$function_name]['iterate'],
          '#description'=>"Iterate the feed by incrementing this value",
          '#options'=>$iterate_options,
        );
        $form[$function_name]['iterate_type']=array(
          '#type'=>'select',
          '#title'=>"Iteration method",
          '#options'=>array(
              'date_field'=>"Use date field from result set",
              'alphabetical'=>'Use alphabetical pulling'
          ),
          '#default_value'=>$this->config[$function_name]['iterate_type'],
        );
      }
    }
    
    return $form;
  }
  
}
class NetforumDatefieldIterator extends NetforumIterator{
  function __construct(&$feed, $value){
    $this->feed=&$feed;
    $this->value=$value;
  }
  function next(&$results){
//    $this->value=ord(asc(lower($this->value))+1);

//    $this->value=date_add()
  }
  function is_complete(){
    if($this->value <= now()) {
      return false;
    }
    return true;
  }
}
class NetforumAlphabeticalIterator extends NetforumIterator{
  function __construct(&$feed, $value){
    $this->feed=&$feed;
    $this->value=$value;
  }
  function next(&$results){
    $this->value=ord(asc(lower($this->value))+1);
  }
  function is_complete(){
    if($this->value <= 'z') {
      return false;
    }
    return true;
  }
}

abstract class NetforumIterator{
  private $feed;
  private $value;
  
  function next(&$results){}
  function is_complete(){}
}
