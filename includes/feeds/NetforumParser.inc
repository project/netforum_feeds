<?php


class NetforumParser extends FeedsParser{

  /**
   * Parse content fetched by fetcher.
   *
   * Extending classes must implement this method.
   *
   * @param FeedsSource $source
   *   Source information.
   * @param $fetcher_result
   *   FeedsFetcherResult returned by fetcher.
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result){
    $response = (array)$fetcher_result->getRaw();
    $responses=array();
    foreach($response['Result'] as $index=>$record){
      $responses[]=(array)$record;
    }
    $result = new FeedsParserResult($responses);
    return $result;  
  }

  
  /**
   * Declare the possible mapping sources that this parser produces.
   *
   * @ingroup mappingapi
   *
   * @return
   *   An array of mapping sources, or FALSE if the sources can be defined by
   *   typing a value in a text field.
   *
   *   Example:
   *   @code
   *   array(
   *     'title' => t('Title'),
   *     'created' => t('Published date'),
   *     'url' => t('Feed item URL'),
   *     'guid' => t('Feed item GUID'),
   *   )
   *   @endcode
   */
  public function getMappingSources() {
    self::loadMappers();
    $sources = array(
    );
    // a dry run of the importing process needs to be done in order to get the fields
    // that can be mapped
    $fetcher=feeds_importer($this->id)->fetcher;
    $function=$fetcher->config['retrieve_object'];
    $result=netforum_xweb_request($function, $fetcher->config[$function]['attributes']);
    $records=(string)$result['recordReturn'];
    $first_record=$result->Result[0];
    
    foreach(array_keys((array)$first_record) as $attribute){
      $sources[$attribute] = array(
          'name' => $attribute,
          'description' => $attribute,
      );
    }
    
    return $sources;
  }
  
  /**
   * Get an element identified by $element_key of the given item.
   * The element key corresponds to the values in the array returned by
   * FeedsParser::getMappingSources().
   *
   * This method is invoked from FeedsProcessor::map() when a concrete item is
   * processed.
   *
   * @ingroup mappingapi
   *
   * @param $batch
   *   FeedsImportBatch object containing the sources to be mapped from.
   * @param $element_key
   *   The key identifying the element that should be retrieved from $source
   *
   * @return
   *   The source element from $item identified by $element_key.
   *
   * @see FeedsProcessor::map()
   * @see FeedsCSVParser::getSourceElement()
   */
  public function getSourceElement(FeedsSource $source, FeedsParserResult $result, $element_key) {
  
    switch ($element_key) {
  
      case 'parent:uid':
        if ($source->feed_nid && $node = node_load($source->feed_nid)) {
          return $node->uid;
        }
        break;
      case 'parent:nid':
        return $source->feed_nid;
    }
  
    $item = $result->currentItem();
    return isset($item[$element_key]) ? $item[$element_key] : '';
  }
}